import logging

from gitlabform.gitlab import GitLab
from gitlabform.gitlabform.processors.abstract_processor import AbstractProcessor
from gitlabform.gitlabform.processors.util.decorators import SafeDict
from gitlabform.gitlabform.processors.util.difference_logger import DifferenceLogger


class MergeRequestsProcessor(AbstractProcessor):
    def __init__(self, gitlab: GitLab):
        super().__init__("merge_requests")
        self.gitlab = gitlab

    def _process_configuration(self, project_and_group: str, configuration: dict):
        approvals = configuration.get("merge_requests|approvals")
        if approvals:
            logging.info("Setting approvals settings: %s", approvals)
            self.gitlab.post_approvals_settings(project_and_group, approvals)

        approvers = configuration.get("merge_requests|approvers")
        approver_groups = configuration.get("merge_requests|approver_groups")
        approval_rules = configuration.get("merge_requests|approval_rules") or {}
        approval_rules["Approvers (configured using GitLabForm)"] = {
            'approvers': approvers,
            'approver_groups': approver_groups,
        }

        rules = self.gitlab.get_approvals_rules(project_and_group)
        protected_branches_details = self.gitlab.get_full_protected_branches(project_and_group)

        for approval_rule_name, approval_details in approval_rules.items():
            approvers = approval_details.get('approvers', [])
            approver_groups = approval_details.get('approver_groups', [])
            protected_branches = approval_details.get('protected_branches', [])
            approvals_required = approval_details.get(
                'approvals_required',
                approvals.get('approvals_required', approvals.get('approvals_before_merge')))
            # checking if "is not None" allows configs with empty array to work
            if (
                (approvers is not None or approver_groups is not None)
                and approvals
                and approvals_required
            ):

                # in pre-12.3 API approvers (users and groups) were configured under the same endpoint as approvals settings
                approvals_settings = self.gitlab.get_approvals_settings(project_and_group)
                if (
                    "approvers" in approvals_settings
                    or "approver_groups" in approvals_settings
                ):
                    logging.debug("Deleting legacy approvers setup")
                    self.gitlab.delete_legacy_approvers(project_and_group)

                # is a rule already configured and just needs updating?
                approval_rule_id = None
                for rule in rules:
                    if rule["name"] == approval_rule_name:
                        approval_rule_id = rule["id"]
                        break
                protected_branch_ids = []
                for branch in protected_branches_details:
                    if branch["name"] in protected_branches:
                        protected_branch_ids.append(branch["id"])

                if not approvers:
                    approvers = []
                if not approver_groups:
                    approver_groups = []

                if approval_rule_id:
                    # the rule exists, needs an update
                    logging.info(
                        "Updating approvers rule to users %s and groups %s"
                        % (approvers, approver_groups)
                    )
                    self.gitlab.update_approval_rule(
                        project_and_group,
                        approval_rule_id,
                        approval_rule_name,
                        approvals_required,
                        approvers,
                        approver_groups,
                        protected_branch_ids,
                    )
                else:
                    # the rule does not exist yet, let's create it
                    logging.info(
                        "Creating approvers rule to users %s and groups %s"
                        % (approvers, approver_groups)
                    )
                    self.gitlab.create_approval_rule(
                        project_and_group,
                        approval_rule_name,
                        approvals_required,
                        approvers,
                        approver_groups,
                        protected_branch_ids,
                    )

    def _log_changes(self, project_and_group: str, merge_requests: SafeDict):
        approvals = merge_requests.get("approvals")
        if approvals:
            DifferenceLogger.log_diff(
                "Project %s approvals changes" % project_and_group, dict(), approvals
            )
