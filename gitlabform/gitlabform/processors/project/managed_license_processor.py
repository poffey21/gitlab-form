import logging

from gitlabform.gitlab import GitLab
from gitlabform.gitlabform.processors.abstract_processor import AbstractProcessor


class ManagedLicenseProcessor(AbstractProcessor):
    def __init__(self, gitlab: GitLab):
        super().__init__("managed_licenses")
        self.gitlab = gitlab

    def _process_configuration(
        self, project_and_group: str, configuration: dict, do_apply: bool = True
    ):
        for managed_license in sorted(configuration["managed_licenses"]):

            if configuration.get("managed_licenses|" + managed_license + "|delete"):
                managed_license_id = self.gitlab.get_managed_license_id(project_and_group, managed_license)
                if managed_license_id:
                    logging.debug("Deleting managed license '%s'", managed_license)
                    self.gitlab.delete_managed_license(project_and_group, managed_license_id)
                else:
                    logging.debug(
                        "Not deleting managed license '%s', because it doesn't exist", managed_license
                    )
            else:
                managed_license_id = self.gitlab.get_managed_license_id(project_and_group, managed_license)
                if managed_license_id:
                    logging.debug("Changing existing managed license '%s'", managed_license)
                    self.gitlab.put_managed_license(
                        project_and_group, managed_license_id, managed_license, configuration["managed_licenses"][managed_license]
                    )
                else:
                    logging.debug("Creating managed license '%s'", managed_license)
                    self.gitlab.post_managed_license(
                        project_and_group, managed_license, configuration["managed_licenses"][managed_license]
                    )
