FROM python:slim
RUN apt-get update \
    && apt-get install -y pandoc \
    && apt-get clean
RUN pip3 install --no-cache-dir pypandoc envtpl
COPY . /gitlabform
RUN cd gitlabform && python setup.py develop
WORKDIR /config